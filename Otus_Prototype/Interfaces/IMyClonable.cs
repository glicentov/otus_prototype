﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Prototype.Interfaces
{
    public interface IMyCLonable<T>
    {
        T GetСlone();
    }
}
