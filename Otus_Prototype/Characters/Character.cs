﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Otus_Prototype.Interfaces;

namespace Otus_Prototype
{
    /// <summary>
    ///  В рамках задания будет создана иерархия персонажей в некой игре
    ///  Базоый класс Character от которого наследуются все остальные персонажи
    /// </summary>
    public abstract class Character : IMyCLonable<Character>, ICloneable
    {
        protected int _hp = default;
        protected int _power = default;
        protected int _level = default;
        protected string _name = default;

        protected int HP { get; set; }

        public abstract object Clone();
        public abstract Character GetСlone();

        public Character() { }
    }
}
