﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Prototype.Characters
{
    public class Guard : Character
    {
        public override Guard GetСlone()
        {
            return new Guard();
        }

        public override object Clone()
        {
            return new Guard();
        }
        public Guard()
        {
            _hp = 20;
            _power = 20;
            _level = 0;
            _name = "NoLevel";
        }
    }
}
