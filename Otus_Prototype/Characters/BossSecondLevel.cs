﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Prototype
{
    public class BossSecondLevel : Character
    {
        public override BossSecondLevel GetСlone()
        {
            return new BossSecondLevel();
        }

        public override object Clone()
        {
            return new BossSecondLevel();
        }

        public BossSecondLevel()
        {
            _hp = 70;
            _power = 70;
            _level = 2;
            _name = "SecondLevelBoss";
        }

    }
}
