﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Otus_Prototype
{
    public class BossFirstLevel : Character
    {

        public override object Clone()
        {
            return new BossFirstLevel();
        }
        public override BossFirstLevel GetСlone()
        {
            return new BossFirstLevel();
        }

        public BossFirstLevel()
        {
            _hp = 50;
            _power = 50;
            _level = 1;
            _name = "FirstLevelBoss";
        }

    }
}
