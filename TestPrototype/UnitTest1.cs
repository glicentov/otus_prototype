﻿using Otus_Prototype;
using Microsoft.VisualStudio.TestTools.UnitTesting;
//using Otus_Prototype;
using Otus_Prototype.Characters;

namespace TestPrototype
{
    [TestClass]
    public class TestPrototype
    {
        [TestMethod]
        public void TestGuard() 
        {
            Guard guardSource = new Guard();
            Guard guardCopied = guardSource.GetСlone();
            

            Assert.IsNotNull(guardSource);
            Assert.ReferenceEquals(guardSource, typeof(Character));
            Assert.IsInstanceOfType(guardSource, typeof(Character));

            Assert.IsNotNull(guardCopied);
            Assert.ReferenceEquals(guardCopied, typeof(Character));
            Assert.IsInstanceOfType(guardCopied, typeof(Character));
            
            Assert.AreNotEqual(guardSource, guardCopied);
        }
        [TestMethod]
        public void TestBossFirstLevel()
        {
            BossFirstLevel bflSource = new BossFirstLevel();
            BossFirstLevel bflCopied = bflSource.GetСlone();

            Assert.IsNotNull(bflSource);
            Assert.ReferenceEquals(bflSource, typeof(Character));
            Assert.IsInstanceOfType(bflSource, typeof(Character));

            Assert.IsNotNull(bflCopied);
            Assert.ReferenceEquals(bflCopied, typeof(Character));
            Assert.IsInstanceOfType(bflCopied, typeof(Character));

            Assert.AreNotEqual(bflSource, bflCopied);
        }
    }
}
